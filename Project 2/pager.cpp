#include <iostream>
#include <stdlib.h>
#include <ctime>
#include <fstream>

using namespace std;

const int AREA = 4;
const int TOTAL_MEMORY = 8;

string initFile = "initialFile.txt";
string finalFile = "finalFile.txt";

void checkForDuplicates(int pageTable[], int currPageTableNumber, int index, int count) {
    for (int i = 0; i < count; i++) {
        if (currPageTableNumber == pageTable[i]) {
            pageTable[index] += 1;
            checkForDuplicates(pageTable, pageTable[index], index, count);
        }
    }
}

void inputDataToTextFile(string data[]) {
    ofstream out(initFile);
    
    for (int i = 0; i < AREA; i++) {
        out << data[i] << endl;
    }
}

void inputDataIntoLogicalMemoryFromFile(string logicalMemory[]) {
    ifstream file(initFile);
    string str;
    int count = 0;
    while (getline(file, str)) {
        if (!str.empty()) {
            logicalMemory[count] = str;
        }
        count++;
    }
}

void inputDataIntoPhysicalMemoryFromLogMem(int pageTable[], string logicalMemory[], string physicalMemory[]) {
    for (int i = 0; i < AREA; i++) {
        physicalMemory[pageTable[i]] = logicalMemory[i];
    }
}

void inputDataIntoFileFromPhysMem(string physicalMemory[], string logicalMemory[]) {
    int index;

    ofstream out(finalFile);

    for (int i = 0; i < TOTAL_MEMORY; i++) {
        if (!physicalMemory[i].empty()) {

            // find index 
            for (int j = 0; j < AREA; j++) {
                if (physicalMemory[i] == logicalMemory[j]) {
                    index = j;
                }
            }

            out << "Index Number: " << index << " - " << "Frame Number " << i << ": " << physicalMemory[i] << endl;
        }
    }

    // explain technique that was used
    out << "Technique Used: Arrays were used to store the data and for the page table" << endl;
}

void randomizePageTable(int pageTable[]) {
    int count = 0;

    // initialize random seed
    srand(time(0));

    for (int i = 0; i < AREA; i++) {
        pageTable[i] = rand() % (TOTAL_MEMORY - 2);
        checkForDuplicates(pageTable, pageTable[i], i, count);
        count++;
    }
}

int main() {
    // starter page table
    int pageTable[AREA];    

    // data of 4 byte strings 
    string data[AREA] = { "Deku", "Mina", "Dabi", "Toga"};

    // logical memory and physical memory
    string logicalMemory[AREA];
    string physicalMemory[TOTAL_MEMORY]; 

    // --- create files ---
    string touch = "touch " + initFile;
    system((char*)touch.c_str());

    touch = "touch " + finalFile;
    system((char*)touch.c_str());

    // ---- start sequence ----
    inputDataToTextFile(data);
    inputDataIntoLogicalMemoryFromFile(logicalMemory);
    randomizePageTable(pageTable);
    inputDataIntoPhysicalMemoryFromLogMem(pageTable, logicalMemory, physicalMemory);
    inputDataIntoFileFromPhysMem(physicalMemory, logicalMemory);

    // display last file
    string cat = "cat " + finalFile;
    system((char*)cat.c_str());

    // delete the original file
    string rm = "rm " + initFile;
    system((char*)rm.c_str());
}