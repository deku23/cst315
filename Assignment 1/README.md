-----Assignment 1--------


In this assignment, we familiarized ourselves with a typical producer and consumer problem using pthreads and mutexes. In the Assigment 1 folder, it includes my producer-consumer.cpp file which demonstrates a producer and consumer.

To run this file, clone the repo down to your local system. Open a linux operating system on your computer and open the directory where you cloned the repo. Step into the assignment 1 folder. Then type this command to compile the file 

g++ -o producer-consumer producer-consumer.cpp -lpthread

Then you can simply run the file by typing this command

./producer-consumer