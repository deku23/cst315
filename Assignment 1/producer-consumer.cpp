#include <thread>
#include <iostream>
#include <mutex>
#include <queue> 

using namespace std; 

mutex m;
queue<string> buffer;

// a list of "random" names for the buffer 
string names[10] = {
    "Naruto",
    "Sasuke",
    "Sakura",
    "Kakashi",
    "Minato",
    "Kushina",
    "Tsnuade",
    "Jiraiya",
    "Itachi",
    "Hinata" 
};

void consumer() {
    cout << "Consumer Thread ID - " << this_thread::get_id() << endl;
    string value;
    bool run = true;

    while (run) {

        if (buffer.empty()) {
            cout << "buffer is empty" << endl; //is checking each time if the queue is empty
        } else {
            value = buffer.front();
            if (value != "END") {
                cout << "popping value " << value << endl;
                m.lock(); // the locking and unlocking of the mutex makes sure only one thread is using the buffer
                buffer.pop(); //(FIFO) logic. The first thing put into the que is the first thing popped out of the queue
                m.unlock();
            } else {
                run = false;
            }
        }

        this_thread::sleep_for(chrono::milliseconds(1000)); // this sleeps the thread for 1 second
    }

}

void producer() {

    cout << "Producer Thread ID - " << this_thread::get_id() << endl;
    for (int i = 0; i < 10; i++) {
        cout << "pushing value to buffer " << names[i] << endl;
        m.lock();
        buffer.push(names[i]); //pushes the names from the list of names to our buffer
        m.unlock();
        this_thread::sleep_for(chrono::milliseconds(2000));
    }

    buffer.push("END");

}

int main() {
    cout << "Main Thread ID - " << this_thread::get_id() << endl;
    // this creates the threads for producer and consumer
    thread thread1(producer);
    thread thread2(consumer);

    //this starts the threads
    thread1.join();
    thread2.join();

    return 0;
}
