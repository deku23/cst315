-----Assignment 2--------


In this assignment, we familiarized ourselves with a typical producer and consumer problem using monitors and semaphores. In the Assigment 2 folder, it includes my monitors.cpp file and the semaphores.cpp which demonstrates a producer and consumer problem using monitor or semaphores.

To run this file, clone the repo down to your local system. Open a linux operating system on your computer and open the directory where you cloned the repo. Step into the assignment 2 folder. Then type this command to compile the file 


----Monitors-----
g++ -o monitors monitors.cpp -lpthread

Then you can simply run the file by typing this command

./monitors



----Semaphores-----
g++ -o semaphores semaphores.cpp -lpthread

Then you can simply run the file by typing this command

./semaphores