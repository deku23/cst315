#include <thread>
#include <iostream>
#include <queue> 

using namespace std; 

queue<string> buffer;

int semaphore; 

void wait(int semaphore) {
    while(semaphore <= 0) {
        //waits constantly until the semaphore is higher then 0 
    }
    semaphore--;
}

void signal(int semaphore) {
    semaphore++;
}

// a list of "random" names for the buffer 
string names[10] = {
    "Harry",
    "Hermione",
    "Ron",
    "Snape",
    "Dumbledore",
    "Luna",
    "Ginny",
    "Lily",
    "James",
    "Sirius"
};

void consumer() {
    string value;
    bool run = true;

    while (run) {
        this_thread::sleep_for(chrono::milliseconds(1000)); // this sleeps the thread for 1 second
        if (buffer.empty()) {
            cout << "buffer is empty" << endl; //is checking each time if the queue is empty
        } else {
            value = buffer.front();
            if (value != "END") {
                cout << "popping value " << value << endl;
                wait(semaphore);
                buffer.pop(); //(FIFO) logic. The first thing put into the que is the first thing popped out of the queue
                signal(semaphore);
            } else {
                run = false;
            }
        }
    }

    cout << "Complete!!" << endl;

}

void producer() {
    for (int i = 0; i < 10; i++) {
        cout << "pushing value to buffer " << names[i] << endl;
        wait(semaphore);
        buffer.push(names[i]); //pushes the names from the list of names to our buffer
        signal(semaphore);
        this_thread::sleep_for(chrono::milliseconds(2000)); // this sleeps the thread for 1 second
    }
    buffer.push("END");

}

int main() {
    // this creates the threads for producer and consumer
    thread thread1(producer);
    thread thread2(consumer);
 
    semaphore = 1; // this shows we have one resource at the start

    thread1.join();
    thread2.join();

    return 0;
}
