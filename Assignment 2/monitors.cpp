#include <thread>
#include <iostream>
#include <queue> 

using namespace std; 

queue<string> buffer;

class Monitor {
    public:
        void producer();
        void consumer();

    private: 
        int count = 1;
        string names[10] = {
            "Hawkeye",
            "Thor",
            "Black Widow",
            "Hulk",
            "Iron Man",
            "Spider-Man",
            "Captain America",
            "Wanda",
            "Vision",
            "Loki"
        };

        void wait(int count) {
            while(count <= 0) {
                //waits for count to be higher then 0
            }
            count--;
        }

        void signal(int count) {
            count++;
        }
};

void Monitor::consumer() {
    string value;
    bool run = true;

    while (run) {
        this_thread::sleep_for(chrono::milliseconds(3000)); // this sleeps the thread for 1 second
        if (buffer.empty()) {
            cout << "buffer is empty" << endl; //is checking each time if the queue is empty
        } else {
            value = buffer.front();
            if (value != "END") {
                cout << "popping value " << value << endl;
                wait(count);
                buffer.pop(); //(FIFO) logic. The first thing put into the que is the first thing popped out of the queue
                signal(count);
            } else {
                run = false;
            }
        }
    }
    cout << "Complete!" << endl;

}

void Monitor::producer() {
    for (int i = 0; i < 10; i++) {
        cout << "pushing value to buffer " << names[i] << endl;
        wait(count);
        buffer.push(names[i]); //pushes the names from the list of names to our buffer
        signal(count);
        this_thread::sleep_for(chrono::milliseconds(2000));
    }

    buffer.push("END");

}

int main() {
    // this creates the threads for producer and consumer
    Monitor * m = new Monitor();

    thread thread1(&Monitor::producer, m);
    thread thread2(&Monitor::consumer, m);

    thread1.join();
    thread2.join();

    return 0;
}
