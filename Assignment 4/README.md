-----Assignment 4--------

In this assignment, we have created our command injection virus in our "Turtle Shell" and a malware detection program. 

To run this file, clone the repo down to your local system. Open a linux operating system on your computer and open the directory where you cloned the repo. Step into the Assignment 4 folder. 

You need to have boost library installed to run this program. Before running type this command into your terminal.

sudo apt-get install libboost-all-dev

Then type this command to compile the shell file

g++ -o turtleshell turtleshell.cpp 

then type this command to compile the malware detection file

g++ -o malwareDetection malwareDetection.cpp 

Then you can simply run the file by typing this command

./turtleshell

Then in the turtle shell or outside in your own shell you can run the malwareDetection by typing 
./malwareDetection

Special Commands to this "Turtle Shell"
    To exit the terminal, type the command "quit"
    To end the execution use the key combo ctrl-c
    