#include <thread>
#include <iostream>
#include <mutex>
#include <queue>

using namespace std;

const int ARRAY_SIZE = 5;

mutex m1; // for the first resource
mutex m2; // for the second resource

// multiple resources
queue<int> initialBuffer;
queue<int> finalBuffer;

// initial data entry
int numbers[ARRAY_SIZE] = {1, 2, 3, 4, 5};

void consumer() {
    int value;
    bool run = true;

    while (run) {
        this_thread::sleep_for(chrono::milliseconds(5000)); // this sleeps the thread for 4 seconds
        cout << "[Consumer Process]: Running" << endl;
        if (finalBuffer.empty()) {
            cout << "[Consumer Process]: Second Resouce is Empty" << endl; //is checking each time if the queue is empty
        } else {
            value = finalBuffer.front();
            if (value != -1) {
                cout << "[Consumer Process]: Popping Value from Second Resource " << value << endl;
                m2.lock(); // the locking and unlocking of the mutex makes sure only one thread is using the buffer
                finalBuffer.pop(); //(FIFO) logic. The first thing put into the que is the first thing popped out of the queue
                m2.unlock();
            } else {
                cout << "[Consumer Process]: Finished" << endl;
                run = false;
            }
        }
    }

}

void calculator() {
    bool run = true;

    while (run) {
        this_thread::sleep_for(chrono::milliseconds(4000)); // this sleeps the thread for 2 seconds
        cout << "[Calculator Process]: Running" << endl;
        if (initialBuffer.empty()) {
            cout << "[Calculator Process]: First Resource is Empty" << endl;
        } else {
            int value = initialBuffer.front();

            if (value != -1) {
                cout << "[Calculator Process]: Popping Value from First Resource " << value << endl;
                m1.lock();
                initialBuffer.pop(); //(FIFO) logic. The first thing put into the que is the first thing popped out of the queue
                m1.unlock();

                // "random calculation"
                value *= 5;

                cout << "[Calculator Process]: Pushing Calculated Value to Second Resource " << value << endl;
                m2.lock();
                finalBuffer.push(value);
                m2.unlock();
            } else {
                m2.lock();
                finalBuffer.push(-1); // ends the program
                m2.unlock();
                cout << "[Calculator Process]: Finished" << endl;
                run = false;
            }
        }
    }

}

void producer() {
    for (int i = 0; i < ARRAY_SIZE; i++) {
        cout << "[Producer Process]: Running" << endl;
        cout << "[Producer Process]: Pushing Value To First Resource " << numbers[i] << endl;
        m1.lock();
        initialBuffer.push(numbers[i]); //pushes the numbers from the list of numbers to our buffer
        m1.unlock();
        this_thread::sleep_for(chrono::milliseconds(1000)); // this sleeps the thread for 1 second.
    }
    cout << "[Producer Process]: Finished" << endl;
    initialBuffer.push(-1);

}

int main() {
    // this creates the threads for producer, calculator, and consumer
    thread thread1(producer);
    thread thread2(calculator);
    thread thread3(consumer);

    //this starts the threads
    thread1.join();
    thread2.join();
    thread3.join();

    return 0;
}