-----Assignment 3--------

In this assignment, we familiarized ourselves with a typical deadlock avoidance problem. In the Assigment 3 folder, it includes the deadlock.cpp file.

To run this file, clone the repo down to your local system. Open a linux operating system on your computer and open the directory where you cloned the repo. Step into the assignment 3 folder. Then type this command to compile the file

g++ -o deadlock deadlock.cpp -lpthread

Then you can simply run the file by typing this command

./deadlock
