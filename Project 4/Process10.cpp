#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process10 : public ProcessBase {
    public:
        void process10() {
            while (!processCompleted) {
                if (state == RUN) {
                    // User process
                    int base = 8; 
                    int height = 7; 
                    int area = base * height;

                    cout << "Process 10: " << "The area of a Parallelogram with a base (" << base << ") and height (" << height << ") is: " << area << endl;
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
};
