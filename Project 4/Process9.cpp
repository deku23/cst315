#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>
#include <cmath>

using namespace std;
using namespace StateMachine;

class Process9 : public ProcessBase {
    public:
        void process9() {
            while(!processCompleted) {
                if (state == RUN) {
                    // User process
                    int radius = 5;
                    int pi = M_PI;
                    int area = pi * pow(radius, 2);

                    cout << "Process 9: " << "The area of a Circle with radius (" << radius << ") is: " << area << endl;  
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }     
        }
};
