#include <iostream>
#include <thread>
#include <queue>
#include "Common.hpp"
#include "Process1.cpp"
#include "Process2.cpp"
#include "Process3.cpp"
#include "Process4.cpp"
#include "Process5.cpp"
#include "Process6.cpp"
#include "Process7.cpp"
#include "Process8.cpp"
#include "Process9.cpp"
#include "Process10.cpp"
#include <cmath>
#include <vector>

using namespace std;
using namespace StateMachine;

int main() {
    // create the instances of the classes 
    Process1 * p1 = new Process1();
    Process2 * p2 = new Process2();
    Process3 * p3 = new Process3();
    Process4 * p4 = new Process4();
    Process5 * p5 = new Process5();
    Process6 * p6 = new Process6();
    Process7 * p7 = new Process7();
    Process8 * p8 = new Process8();
    Process9 * p9 = new Process9();
    Process10 * p10 = new Process10();

    vector<ProcessBase *> processes;

    processes.push_back(p1);
    processes.push_back(p2);
    processes.push_back(p3);
    processes.push_back(p4);
    processes.push_back(p5);
    processes.push_back(p6);
    processes.push_back(p7);
    processes.push_back(p8);
    processes.push_back(p9);
    processes.push_back(p10);

    // this creates the threads for the processes
    thread thread1(&Process1::process1, p1);
    thread thread2(&Process2::process2, p2);
    thread thread3(&Process3::process3, p3);
    thread thread4(&Process4::process4, p4);
    thread thread5(&Process5::process5, p5);
    thread thread6(&Process6::process6, p6);
    thread thread7(&Process7::process7, p7);
    thread thread8(&Process8::process8, p8);
    thread thread9(&Process9::process9, p9);
    thread thread10(&Process10::process10, p10);

    // ----- SCHEDULER ---- (FIRST COME FIRST SERVED)

    // start the first process
    processes[0]->state = READY; // switch from wait to ready
    processes[0]->state = RUN; // switch from ready to run

    for (int i = 1; i < processes.size(); i++) {
        bool previousProcessCompleted = false;
        while (!previousProcessCompleted) {
            if (processes[i - 1]->state == IDLE) {
                processes[i]->state = READY;
                processes[i]->state = RUN;
                previousProcessCompleted = true;
            }
        }
        previousProcessCompleted = false;
    }
    
    // this makes sure the main thread does not finish before the other processes are completed.
    thread1.join();
    thread2.join();
    thread3.join();
    thread4.join();
    thread5.join();
    thread6.join();
    thread7.join();
    thread8.join();
    thread9.join();
    thread10.join();
}