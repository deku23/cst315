#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process5 : public ProcessBase {
    public:
        void process5() {
            // I/O Process 
            while(!processCompleted) {
                if (state == RUN) {
                    cout << "Process 5: Calling a remote procedure" << endl;

                    // this is to simulate the amount of time it would take to complete this process.
                    this_thread::sleep_for(chrono::milliseconds(1500));
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
};