#include <iostream>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fstream>
#include <boost/algorithm/string.hpp>
#include <signal.h>

using namespace std; 

class CommandLine {
    public:
        string command;
        string argumentStr;
        string commandPathway;
        char* argument;
        
};

void handler(int num) {
    //when the user goes to end execution with "CTRL-C, it will now only end the child process
}

void runCommand(CommandLine commandLine) {
    pid_t pid = fork();
    if (pid < 0) {
        perror("fork failed.");
        exit(1);
    } else if (pid == 0) {
        // Child Process
        char *args[3]; 
        args[0] = (char*)commandLine.commandPathway.c_str(); 
        args[1] = commandLine.argument;
        args[2] = NULL;
        execv(args[0], args);
    } else {
        // Parent Process
        wait(0);
    }
}

bool isValidCommand(CommandLine& commandLine) {
    bool isValidCommand = false;
    
    system("touch output.txt");
    string which = "which " + commandLine.command + " > output.txt"; 
    char *const commandCharArray = (char*)which.c_str(); 

    int check = system(commandCharArray);
    if (check == 0) {
        isValidCommand = true;
        ifstream file("output.txt");
        string str; 
        getline(file, str); 
        commandLine.commandPathway = str; //sets the pathway if it is a 
    } else {
        isValidCommand = false;
    }
    system("rm output.txt");
    return isValidCommand;  
}


bool checkIfBashScript(string segmentOfCommandLine) {
    bool isABashScript = false;
    string bashScript; 
    // last three of commandLine is ".sh"
    if (segmentOfCommandLine.length() > 3) {
        bashScript = segmentOfCommandLine.substr(segmentOfCommandLine.length() - 3);
    }

    if (bashScript == ".sh") {
        isABashScript = true;
    }

    return isABashScript;
}

void changeDirectory(string argument) {
    char *const commandCharArray = (char*)argument.c_str();
    chdir(commandCharArray);
}

void mv(string argument) {
    string mv = "mv " + argument;
    char *const commandCharArray = (char*)mv.c_str();
    system(commandCharArray);
}

void rm(string argument) {
    string rm = "rm " + argument;
    char *const commandCharArray = (char*)rm.c_str();
    system(commandCharArray);
}

void cp(string argument) {
    string cp = "cp " + argument;
    char *const commandCharArray = (char*)cp.c_str();
    system(commandCharArray);
}

void find(string argument) {
    string find = "find " + argument;
    char *const commandCharArray = (char*)find.c_str();
    system(commandCharArray);
}

void truncate(string argument) {
    string truncate = "truncate " + argument;
    char *const commandCharArray = (char*)truncate.c_str();
    system(commandCharArray);
}

void parser(string segment, CommandLine& commandLine) {
    bool hasArgument = false;
    
    vector<string> segmentSplit;

    for (int i = 0; i < segment.length(); i++) {
        int testChar = segment[i];
        if (isspace(testChar)) {
            hasArgument = true;
        }
    }

    if (hasArgument) {
        int spaceIndex = segment.find(" ");

        commandLine.command = segment.substr(0, spaceIndex);
        commandLine.argumentStr = segment.substr(spaceIndex + 1, segment.length() - 1);
        commandLine.argument = &commandLine.argumentStr[0];
    } else {
        commandLine.command = segment;
        commandLine.argument = NULL; //clears out the string
    }
}

void runShell(CommandLine commandLine, string userInput, vector<string> userInputSegmented) {
    // Split the command line based off semicolons.
    boost::split(userInputSegmented, userInput, boost::is_any_of(";")); 

    for (int i = 0; i < userInputSegmented.size(); i++) {
        // check for unneeded space and get rid of it 
        int testChar = userInputSegmented[i][0]; 

        if (isspace(testChar)) { 
            userInputSegmented[i].erase(0, 1);
        }
        // first Check if this is a bash script
        if (checkIfBashScript(userInputSegmented[i])) {
            // if it is a bash script then create another the bash script streaming lines.

            ifstream file(userInputSegmented[i]);
            string str; 
            vector<string> strSegmented; 

            getline(file, str); // gets rid of the first line 

            while (getline(file, str)) {
                cout << str << endl;
                if (!str.empty()) {
                    runShell(commandLine, str, strSegmented); // runs each line again through recursion.
                }  
            }

        } else {
            // if it is not bash script then run the command as normal 
            parser(userInputSegmented[i], commandLine);
            
            // check to see if the user entered a quit. 
            if (commandLine.command == "quit") {
                cout << "Exited Turtle Shell" << endl;
                exit(1);
            } else if(commandLine.command == "cd") {
                changeDirectory(commandLine.argumentStr);
            } else if(commandLine.command == "mv") {
                mv(commandLine.argumentStr);
            } else if(commandLine.command == "rm") {
                rm(commandLine.argumentStr);
            } else if(commandLine.command == "cp") {
                cp(commandLine.argumentStr);
            } else if(commandLine.command == "find") {
                find(commandLine.argumentStr);
            } else if(commandLine.command == "truncate") {
                truncate(commandLine.argumentStr);
            } else if (isValidCommand(commandLine)) { // check if the command actually exists 
                runCommand(commandLine);
            } else {
                cout << "Command not found. Try again" << endl;
            }

        }
    }
}

int main() {
    CommandLine userCommandLine;
    string userInput; 
    vector<string> userInputSegmented;

    signal(SIGINT, handler); // this handles the end execution of a child process

    while(1) { //continues to run the code until specifically the user stops it.
        // Get User Input!!
        cout << "$TURTLE_SHELL >> ";
        getline(cin, userInput);

        // Run the Shell
        runShell(userCommandLine, userInput, userInputSegmented);   
    }

    return 0;
}