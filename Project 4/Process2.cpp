#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process2 : public ProcessBase {
    public: 
        void process2() {
            // I/O Process 
            while(!processCompleted) {
                if (state == RUN) {
                    cout << "Process 2: Sending data to Printer" << endl;

                    // this is to simulate the amount of time it would take to complete this process.
                    this_thread::sleep_for(chrono::milliseconds(3000));
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
        
};