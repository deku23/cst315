-----PROJECT 4--------

In this project, we have created our version of the shell and a scheduler. It can run all of the common commands found in a Linux Shell and our shell runs the scheduler. 

To run this file, clone the repo down to your local system. Open a linux operating system on your computer and open the directory where you cloned the repo. Step into the project 4 folder. 

You need to have boost library installed to run this program. Before running type this command into your terminal.

sudo apt-get install libboost-all-dev

You will also need the tree command 

sudo apt install tree

Then type this command to compile the shell file

g++ -o turtleshell turtleshell.cpp 

then type this command to compile the scheduler file

g++ -o scheduler scheduler.cpp -lpthread

Then you can simply run the file by typing this command

./turtleshell

Then in the turtle shell you can run the scheduler by typing ./scheduler

Special Commands to this "Turtle Shell"
    To exit the terminal, type the command "quit"
    To end the execution use the key combo ctrl-c
    