#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process7 : public ProcessBase {
    public:
        void process7() {
            while(!processCompleted) {
                if (state == RUN) {
                    // User process
                    int a = 78; 
                    int b = 20; 
                    int result = a * b; 

                    cout << "Process 7: " << "The product of a (" << a << ") * b (" << b << "): " << result << endl;
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
};
 