#ifndef PROCESSBASE_H 
#define PROCESSBASE_H

#include "Common.hpp"
#include <iostream>
#include <thread>

using namespace std;
using namespace StateMachine;

class ProcessBase {
    public:
        enum states state = WAITING;
        bool processCompleted = false;
};

#endif