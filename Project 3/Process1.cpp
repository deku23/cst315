#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process1 : public ProcessBase {
    public: 
        void process1() {
            // I/O Process 
            while(!processCompleted) {
                if (state == RUN) {
                    cout << "Process 1: Reading From a File" << endl;

                    // this is to simulate the amount of time it would take to complete this process.
                    this_thread::sleep_for(chrono::milliseconds(2000));  
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
};
