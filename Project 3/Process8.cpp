#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process8 : public ProcessBase {
    public:
        void process8() {
            while (!processCompleted) {
                if (state == RUN) {
                    // User process
                    int a = 25; 
                    int b = 100; 
                    int result = b / a; 

                    cout << "Process 8: " << "The quotient of b (" << b << ") / a (" << a << "): " << result << endl;
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }           
};
