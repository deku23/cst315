#ifndef Common_H_
#define Common_H_

namespace StateMachine {
    enum states {
    RUN,
    READY,
    WAITING,
    IDLE
    };
}

#endif