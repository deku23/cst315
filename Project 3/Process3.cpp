#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process3 : public ProcessBase {
    public:
        void process3() {
            while(!processCompleted) {
                if (state == RUN) {
                    cout << "Process 3: Accessing a page from a URL" << endl;

                    // this is to simulate the amount of time it would take to complete this process.
                    this_thread::sleep_for(chrono::milliseconds(1000));
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }      
        }     
};
