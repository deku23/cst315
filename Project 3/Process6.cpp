#include <iostream>
#include "Common.hpp"
#include "ProcessBase.cpp"
#include <thread>

using namespace std;
using namespace StateMachine;

class Process6 : public ProcessBase {
    public: 
        void process6() {
            while(!processCompleted) {
                if (state == RUN) {
                    // User process
                    int a = 5; 
                    int b = 10; 
                    int result = a + b; 

                    cout << "Process 6: " << "The sum of a (" << a << ") + b (" << b << "): " << result << endl;
                    state = IDLE;
                    processCompleted = true;
                }
                this_thread::sleep_for(chrono::milliseconds(500));
            }
        }
};
